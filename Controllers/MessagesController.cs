﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using DevExLambda.Helper;
using DevExLambda.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace DevExLambda.Controllers
{

    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        // GET api/values
        [HttpGet]
        [Route("{name}")]
        public ActionResult<List<Messages>> Get(string name)
        {
            List<ScanCondition> conditions = new List<ScanCondition>();
            conditions.Add(new ScanCondition("UserToId", ScanOperator.Equal, name));
            conditions.Add(new ScanCondition("IsSent", ScanOperator.Equal, false));

            List<Messages> messages = DynamoDBHelper.GetRows<Messages>(conditions).GetAwaiter().GetResult();

            if(messages != null && messages.Count > 0) { 
                foreach (var message in messages)
                {
                    message.IsSent = true;
                }

                DynamoDBHelper.BatchSave(messages).GetAwaiter().GetResult();
            }
            return Ok(messages);
        }

        // POST api/values
        [HttpPost]
        public ActionResult<string> Post([FromBody]Messages tempMessage)
        {
            if (tempMessage != null)
            {
                tempMessage.IsSent = false; 
                DynamoDBHelper.Save<Messages>(tempMessage).GetAwaiter().GetResult();

                return Ok();
            }
            return BadRequest();
        }
    }
}