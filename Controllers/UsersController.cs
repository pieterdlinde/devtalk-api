﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExLambda.Helper;
using DevExLambda.Models;
using Microsoft.AspNetCore.Mvc;

namespace DevExLambda.Controllers
{

    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        // GET api/values
        [HttpGet]
        public ActionResult<List<Users>> Get()
        {
            return Ok(DynamoDBHelper.GetAll<Users>().GetAwaiter().GetResult());
        }

        // POST api/values
        [HttpPost]
        public ActionResult<string> Post([FromBody]Users user)
        {
            if(user != null)
            { 
                DynamoDBHelper.Save<Users>(user).GetAwaiter().GetResult();
                return Ok();
            }

            return BadRequest();
        }
    }
}