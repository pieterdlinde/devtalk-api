﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevExLambda.Models
{
    [DynamoDBTable("Users")]
    public class Users
    {
        public string Id { get; set; }
        public string ImageLocation { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
    }

}
