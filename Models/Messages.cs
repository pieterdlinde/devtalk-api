﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevExLambda.Models
{  
    [DynamoDBTable("Messages")]
    public class Messages
    {
        public string Id { get; set; }
        public string UserFromId { get; set; }
        public string UserFromName { get; set; }
        public string UserToId { get; set; }
        public string UserToName { get; set; }
        public string Message { get; set; }
        public bool IsSent { get; set; }
    }
}
