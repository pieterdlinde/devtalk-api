﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DevExLambda.Helper
{
    public class S3Helper
    {

        public static string bucketName = "devexuserimages";

        public S3Helper()
        {

        }

        public static AmazonS3Client getClient()
        {
            var _AWSS3Key = "AKIA5YIFVXTSA5P65COP";
            var _AWSS3Secret = "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";

            BasicAWSCredentials credentials = new BasicAWSCredentials(_AWSS3Key, _AWSS3Secret);
            return new AmazonS3Client(credentials, RegionEndpoint.EUWest1);
        }

        public static string addUserImage(string username, string name, string base64)
        {
            try
            {
                string cleanImage = Regex.Replace(base64, @"^data:image\/[a-zA-Z]+;base64,", string.Empty);

                PutObjectResponse value;
                byte[] bytes = Convert.FromBase64String(cleanImage);

                PutObjectRequest putRequest = new PutObjectRequest();
                putRequest.BucketName = bucketName;
                putRequest.Key = username + "/avatar/" + name;
                putRequest.ContentType = "image/jpeg";

                using (var ms = new MemoryStream(bytes))
                {
                    putRequest.InputStream = ms;
                    value = getClient().PutObjectAsync(putRequest).GetAwaiter().GetResult();
                }

                var url = "https://devexuserimages.s3-eu-west-1.amazonaws.com/" + putRequest.Key;

                return url;
            }
            catch (Exception e)
            {
                return "";
            }
        }

    }
}
