﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevExLambda.Helper
{
    public class DynamoDBHelper
    {

        public DynamoDBHelper() {
        
        }

        private static AmazonDynamoDBClient GetClient()
        {
            var _AWSDynamoDBKey = "AKIA5YIFVXTSA5P65COP";
            var _AWSDynamoDBSecret = "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";


            BasicAWSCredentials credentials = new BasicAWSCredentials(_AWSDynamoDBKey, _AWSDynamoDBSecret);
            return new AmazonDynamoDBClient(credentials, RegionEndpoint.EUWest1);

        }


        /// Get all the records from the given table
        public static async Task<List<T>> GetAll<T>()
        {
            var context = new DynamoDBContext(GetClient());

            List<ScanCondition> conditions = new List<ScanCondition>();
            return await context.ScanAsync<T>(conditions).GetRemainingAsync();
        }

        /// Get the rows from the given table which maches the given key and conditions 
        public static async Task<List<T>> GetRows<T>(object keyValue, List<ScanCondition> scanConditions = null)
        {
            var context = new DynamoDBContext(GetClient());
            DynamoDBOperationConfig config = null;

            if (scanConditions != null && scanConditions.Count > 0)
            {
                config = new DynamoDBOperationConfig()
                {
                    QueryFilter = scanConditions
                };
            }
            return await context.QueryAsync<T>(keyValue, config).GetRemainingAsync();
        }


        /// Get the rows from the given table which maches the given conditions 
        public static async Task<List<T>> GetRows<T>(List<ScanCondition> scanConditions)
        {
            var context = new DynamoDBContext(GetClient());
            return await context.ScanAsync<T>(scanConditions).GetRemainingAsync();
        }

        /// Gets a record which matches the given key value
        public static T Load<T>(object keyValue)
        {
            var context = new DynamoDBContext(GetClient());
            return context.LoadAsync<T>(keyValue).Result;
        }


        /// Saves the given record in the table
        public static async Task Save<T>(T document)
        {
            var context = new DynamoDBContext(GetClient());
            await context.SaveAsync(document);
        }


        /// Deletes the given record in the table
        public static async Task Delete<T>(T document)
        {
            var context = new DynamoDBContext(GetClient());
            await context.DeleteAsync(document);
        }


        /// Saves batch of records in the table
        public static async Task BatchSave<T>(IEnumerable<T> documents)
        {
            var context = new DynamoDBContext(GetClient());
            var batch = context.CreateBatchWrite<T>();
            batch.AddPutItems(documents);
            await batch.ExecuteAsync();
        }

        /// Deletes batch of records in the table
        public static async Task BatchDelete<T>(IEnumerable<T> documents)
        {
            var context = new DynamoDBContext(GetClient());
            var batch = context.CreateBatchWrite<T>();
            batch.AddDeleteItems(documents);
            await batch.ExecuteAsync();
        }
    }
}